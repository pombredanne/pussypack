from model import Package
from packageDownloader import Downloader
from packageReader import Reader

dwn = Downloader("http://archive.ubuntu.com/ubuntu/dists/precise", ['main','restricted'], 'i386', './data/precise')

repos = dwn.download()

Package.resetTable()

for repo in repos:
    print "repo", repo
    pkgF = './data/precise/' + repo + "/Packages.bz2"
    pkgR = Reader(pkgF)
    for pkgInfo in pkgR.getPackages():
        if pkgInfo:
            id = Package.create(**pkgInfo)