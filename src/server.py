from util import controller
import sys

application = controller.app

def main():
    port = 7575
    if len(sys.argv) > 1:
        port = sys.argv[1]
    if len(sys.argv) > 2:
        application.run(host='0.0.0.0', port = port, server = sys.argv[2])
    else:
        application.run(host='0.0.0.0', port = port, reloader = True, debug = True)
        
if __name__ == '__main__':
    main()